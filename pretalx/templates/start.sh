#!/bin/bash

set -ex 

export VAR_LIB=/var/lib/pretalx/
mkdir -p $VAR_LIB/{etc,data}
# that location is linked to /etc/pretalx/pretalx.cfg in the container
envsubst < /srv/config/pretalx.cfg > $VAR_LIB/etc/pretalx.cfg
# needed to copy on a RW location
[ -d $VAR_LIB/static/ ] && rm -Rf $VAR_LIB/static/
cp -Rf /srv/src/static.dist/ $VAR_LIB/static/

cd src/
python3 -m pretalx migrate

if [ ! -f ${VAR_LIB}/init ]; then
	python3 -m pretalx init --noinput
	touch ${VAR_LIB}/init
fi;

# done once every restart, since restart happen at least once per week or 2 weeks due to upgrade
python3 -m pretalx clearsessions

# using * do not work, not sure why, guess not using a real bash
gunicorn -b 0.0.0.0:8000 --pythonpath /srv/src/pretalx,$(find /usr/local/ -name site-packages | tr '\n' ',')  pretalx.wsgi

