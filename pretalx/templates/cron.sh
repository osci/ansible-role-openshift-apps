#!/bin/bash
# use a poor's man cron replacement to deal with shared mount point
# as our cluster do not support RW mount unless we deploy Rook or something
while true
do
	python3 -m pretalx runperiodic
	sleep $(( 30 * 60 ))
done
