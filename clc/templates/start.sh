#!/bin/bash
set -x
ls -l /srv/
cd /srv/server
sed -e "s/GITHUB_API_KEY/$GITHUB_API_KEY/" /srv/config/clc.yaml > /srv/scratch/clc.yaml
sed -e "s/ADMIN_PASSWORD/$ADMIN_PASSWORD/" -e "s/READONLY_PASSWORD/$READONLY_PASSWORD/" /srv/config/users.yaml > /srv/scratch/users.yaml
mkdir -p /srv/scratch/git/
python3 main.py --config /srv/scratch/clc.yaml 2>&1
