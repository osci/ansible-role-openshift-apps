#!/bin/bash

set -ex 

# done once every restart, since restart happen at least once per week or 2 weeks due to upgrade
python3 -m pretix clearsessions

# using * do not work, not sure why, guess not using a real bash
gunicorn -b 0.0.0.0:8000 --pythonpath $(find /usr/local/ -name site-packages | tr '\n' ',')  pretix.wsgi
