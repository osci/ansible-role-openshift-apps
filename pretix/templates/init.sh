#!/bin/bash

set -ex 

export VAR_LIB=/var/lib/pretix/
mkdir -p $VAR_LIB/{etc,data}
# that location is linked to /etc/pretix/pretix.cfg in the container
envsubst < /srv/config/pretix.cfg > $VAR_LIB/etc/pretix.cfg

# needed to copy on a RW location
[ -d $VAR_LIB/static.dist/ ] && rm -Rf $VAR_LIB/static.dist/
D=$(ls -d /usr/local/lib/python3.*/site-packages/pretix/)
cp -Rf $D/static.dist.orig $VAR_LIB/static.dist

python3 -m pretix migrate
python3 -m pretix compress


